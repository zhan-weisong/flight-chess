/*
 * Copyright (c) 2022 LookerSong
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import MapData from '../../common/MapData.js';

var onestep;        // 重复定时器声明
var temp;       // 临时骰子点数（重复定时器执行次数）声明
var thetype;        // 当前回合颜色
var forward = 0;        // 前进步数
var backward = 0;       // 后退步数
var jump1;      // 第一次位移判断执行定时器
var jump2;      // 第二次位移判断执行定时器
var ruzhan;     // 将棋子写入地图执行定时器
var changeturn      // 回合轮替执行定时器

// 各色飞机的飞行航线
let Route = [
    [76, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
    30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 52, 53, 54, 55, 56, 57],    // 红线
    [81, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 58, 59, 60, 61, 62, 63],  // 绿线
    [86, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 64, 65, 66, 67, 68, 69], // 黄线
    [91, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    16, 17,18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 70, 71, 72, 73, 74, 75]  // 蓝线
]

// 游戏大致流程：判断当前玩家是否获胜，否则投骰子
// 骰子不可再次点击，开启棋子可点击
// 选择棋子移动，关闭棋子可点击
// 移动结束确定回合，开启骰子可点击

export default {
    data: {
        roundtitle: "",     // 某方回合播报
        theround: 0,        // 当前回合数
        dice_pic: "dice",        // 骰子图片
        dice_num: 0,        // 骰子点数
        dice_dab: false,        // 骰子可交互
        result: false,      // 游戏结束弹窗显示标识符
        flylog: [       // 飞行记录
            {
                camp: "common/red.png",     // 阵营图标
                hit: 0,     // 击落敌机数量
                progress: 0,        // 抵达终点的飞机数 n/4
            },
            {
                camp: "common/green.png",
                hit: 0,
                progress: 0,
            },
            {
                camp: "common/yellow.png",
                hit: 0,
                progress: 0,
            },
            {
                camp: "common/blue.png",
                hit: 0,
                progress: 0,
            },
        ],
        allrank: [      // 游戏排行榜
            //            {
            //                rank: 1,        // 排名
            //                chess: "",      // 飞行棋图标地址
            //                round: "",      // 用时n回合 或 未完成
            //            },
        ],
        RED: [
            {
                index: 77,        // 初始棋格下标
                type: "wait",       // 棋子状态：待机，可移动，抵达
                chess_dab: true,        // 棋子不可点击
                step: null,     // 当前航行步数进度
                x: 78.15,      // X轴（百分比）
                y: 78.35,     // Y轴（百分比）
                angle: 270,       // 棋子朝向
                color: "red",      // 棋子颜色
            },
            {
                index: 78,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 78.15,
                y: 86.6,
                angle: 270,
                color: "red",
            },
            {
                index: 79,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 86.65,
                y: 78.35,
                angle: 270,
                color: "red",
            },
            {
                index: 80,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 86.55,
                y: 86.5,
                angle: 270,
                color: "red",
            }
        ],
        GREEN: [
            {
                index: 82,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 5.8,
                y: 78.2,
                angle: 0,
                color: "green",
            },
            {
                index: 83,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 5.8,
                y: 86.5,
                angle: 0,
                color: "green",
            },
            {
                index: 84,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 14.1,
                y: 78.1,
                angle: 0,
                color: "green",
            },
            {
                index: 85,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 14.1,
                y: 86.4,
                angle: 0,
                color: "green",
            }
        ],
        YELLOW: [
            {
                index: 87,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 5.7,
                y: 5.8,
                angle: 90,
                color: "yellow",
            },
            {
                index: 88,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 5.7,
                y: 13.9,
                angle: 90,
                color: "yellow",
            },
            {
                index: 89,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 14.1,
                y: 5.8,
                angle: 90,
                color: "yellow",
            },
            {
                index: 90,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 14.1,
                y: 13.8,
                angle: 90,
                color: "yellow",
            }
        ],
        BLUE: [
            {
                index: 92,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 78.3,
                y: 5.6,
                angle: 180,
                color: "blue",
            },
            {
                index: 93,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 78.3,
                y: 14.1,
                angle: 180,
                color: "blue",
            },
            {
                index: 94,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 86.55,
                y: 5.6,
                angle: 180,
                color: "blue",
            },
            {
                index: 95,
                type: "wait",
                chess_dab: true,
                step: null,
                x: 86.45,
                y: 14.1,
                angle: 180,
                color: "blue",
            }
        ]
    },

    onInit() {
        for(var i=0; i<4; i++) {
            MapData[77+i].chess.push(this.RED[i]);
            MapData[82+i].chess.push(this.GREEN[i]);
            MapData[87+i].chess.push(this.YELLOW[i]);
            MapData[92+i].chess.push(this.BLUE[i]);
        }

        // 默认红色先手
        thetype = this.RED;
        this.roundtitle = "红色方的回合";
    },

    // 掷骰子
    // 是否4个棋子都到达终点，否则投骰子，是则直接回合数加一，轮到下一玩家；
    // 骰子点数为1~5，无可移动棋子，直接回合数加一，轮到下一玩家；
    // 骰子点数为1~5，有可移动棋子，玩家选择棋子移动…… 回合数加一，轮到下一玩家；
    // 骰子点数为6，有初始棋子或可移动棋子，选择棋子出发或者移动…… 再投一次骰子……
    todice() {
        this.dice_dab = true;
        this.dice_num = Math.floor(Math.random()*6+1);
        switch(this.dice_num) {
            case 1:
                this.dice_pic = "point1";
                break;
            case 2:
                this.dice_pic = "point2";
                break;
            case 3:
                this.dice_pic = "point3";
                break;
            case 4:
                this.dice_pic = "point4";
                break;
            case 5:
                this.dice_pic = "point5";
                break;
            case 6:
                this.dice_pic = "point6";
                break;
            default:
                console.log("骰子意外出错");
                break;
        }
        // 骰子点数小于6，若有飞行状态的棋子可点击，该回合可操作，否则回合轮替
        if(6 > this.dice_num) {
            var operable = false;
            for(var i=0; i<4; i++) {
                if("flying" == thetype[i].type) {
                    thetype[i].chess_dab = false;
                    operable = true;
                }
            }
            if(false == operable) {
                this.rotate();
            }
            else {}
        }
        // 骰子点数为6，除已到达的棋子都可点击
        else {
            for(var i=0; i<4; i++) {
                if("arrive" != thetype[i].type) {
                    thetype[i].chess_dab = false;
                }
            }
        }
    },

    // 选中棋子行动
    appoint(thecamp, num) {
        for(var i=0; i<4; i++) {
            thecamp[i].chess_dab = true;
        }
        // 若该棋子已进入航线
        if(null != thecamp[num].step) {
            for(var t=0; t<MapData[Route[this.theround%4][thecamp[num].step]].chess.length; t++) {
                if(thecamp[num].index == MapData[Route[this.theround%4][thecamp[num].step]].chess[t].index) {
                    MapData[Route[this.theround%4][thecamp[num].step]].chess.splice(t, 1);
                    break;
                }
            }
        }
        // 如果该棋子处于待机状态，进入起点，最后结束
        if("wait" == thecamp[num].type) {
            MapData[thecamp[num].index].chess.pop();
            thecamp[num].step = 0;
            thecamp[num].type = "flying";
            thecamp[num].x = MapData[Route[this.theround%4][thecamp[num].step]].x;
            thecamp[num].y = MapData[Route[this.theround%4][thecamp[num].step]].y;
            thecamp[num].angle = MapData[Route[this.theround%4][thecamp[num].step]].angle;
            MapData[Route[this.theround%4][thecamp[num].step]].chess.push(thecamp[num]);
            this.dice_num = 0;
            this.dice_dab = false;
            this.dice_pic = "dice";
            return;
        }
        temp = this.dice_num;
        // 若走不到终点
        if(56 >= (thecamp[num].step + this.dice_num)) {
            forward = temp;
        }
        // 超过终点，回退几步
        else {
            forward = 56 - thecamp[num].step;
            backward = temp - forward;
        }
        // 0.5秒执行一次走棋方法
        onestep = setInterval(()=> {
            this.move(thecamp[num]);
        }, 500);
    },

    // 移动棋子
    move(thechess) {
        // 若前进步数为0，且需要后退
        if((0 == forward) && (0 != backward)) {
            thechess.step -= 1;
            backward --;
        }
        // 若需要前进
        if(forward != 0) {
            thechess.step += 1;
            forward --;
        }
        thechess.x = MapData[Route[this.theround%4][thechess.step]].x;
        thechess.y = MapData[Route[this.theround%4][thechess.step]].y;
        thechess.angle = MapData[Route[this.theround%4][thechess.step]].angle;
        temp -= 1;

        // 若步数走完
        if(0 == temp) {
            clearInterval(onestep);
            forward = 0;
            backward = 0;
            this.complex(thechess);     // 踩棋子判断
            this.getjump(thechess);     // 位移判断

            // 向棋子当前落点写入棋子信息
            ruzhan = setTimeout(()=> {
                MapData[Route[this.theround%4][thechess.step]].chess.push(thechess);
            }, 1200);

            // 延迟后进行回合轮替
            changeturn = setTimeout(()=> {
                // 若该棋子到达终点，更新进度
                if(56 == thechess.step) {
                    thechess.type = "arrive";
                    this.flylog[this.theround%4].progress += 1;

                    // 若该棋子走完后刚好全部到达，计入排行榜
                    if(4 == this.flylog[this.theround%4].progress) {
                        this.allrank.push(
                            {
                                rank: this.allrank.length + 1,
                                chess: this.flylog[this.theround%4].camp,
                                round: "用时" + this.theround + "回合",
                            }
                        )
                        if(3 == this.allrank.length) {
                            for(var i=0; i<4; i++) {
                                if(this.flylog[i].progress < 4) {
                                    var chesstemp = this.flylog[i].camp;
                                }
                            }
                            this.allrank.push(
                                {
                                    rank: this.allrank.length + 1,
                                    chess: chesstemp,
                                    round: "未完成",
                                }
                            )
                            this.dice_dab = true;
                            this.result = true;
                            return;
                        }
                    }
                }
                this.rotate();
            }, 1500);
        }
    },

    // 回合轮替
    rotate() {
        // 刚刚是否投出6，是则再来一次，否则回合数加一，进行轮替
        if(6 == this.dice_num) {
            if(4 == this.flylog[this.theround%4].progress) {
                this.theround += 1;
            }
        }
        else {
            this.theround += 1;
        }
        this.dice_num = 0;
        this.dice_pic = "dice";
        this.dice_dab = false;

        switch(this.theround % 4) {
            case 0:     // 红的回合
                thetype = this.RED;
                this.roundtitle = "红色方的回合";
                break;
            case 1:     // 绿的回合
                thetype = this.GREEN;
                this.roundtitle = "绿色方的回合";
                break;
            case 2:     // 黄的回合
                thetype = this.YELLOW;
                this.roundtitle = "黄色方的回合";
                break;
            case 3:     // 蓝的回合
                thetype = this.BLUE;
                this.roundtitle = "蓝色方的回合";
                break;
            default:
                console.log("意外出错");
                break;
        }

        // 若该颜色的4枚棋子都已到达终点，直接进行回合轮替
        var win = 0;
        for(var i=0; i<4; i++) {
            if("arrive" == thetype[i].type) {
                win += 1;
            }
        }
        if(4 == win) {
            this.rotate();
        }
    },

    // 落点是否有棋子
    complex(thechess) {
        if(52 > MapData[Route[this.theround%4][thechess.step]].index) {
            if(0 != MapData[Route[this.theround%4][thechess.step]].chess.length) {
                // 我方棋子
                if(thechess.color == MapData[Route[this.theround%4][thechess.step]].chess[0].color) {
                }
                // 敌方棋子，踩回起点
                else {
                    for(var i=0; i<MapData[Route[this.theround%4][thechess.step]].chess.length; i++) {
                        MapData[Route[this.theround%4][thechess.step]].chess[i].type = "wait";
                        MapData[Route[this.theround%4][thechess.step]].chess[i].step = null;
                        MapData[Route[this.theround%4][thechess.step]].chess[i].x =
                        MapData[MapData[Route[this.theround%4][thechess.step]].chess[i].index].x;
                        MapData[Route[this.theround%4][thechess.step]].chess[i].y =
                        MapData[MapData[Route[this.theround%4][thechess.step]].chess[i].index].y;
                        MapData[Route[this.theround%4][thechess.step]].chess[i].angle =
                        MapData[MapData[Route[this.theround%4][thechess.step]].chess[i].index].angle;
                        this.flylog[this.theround%4].hit += 1;
                    }
                    MapData[Route[this.theround%4][thechess.step]].chess.splice(0, MapData[Route[this.theround%4][thechess.step]].chess.length);
                }
            }
        }
    },

    // 判断触发位移
    getjump(thechess) {
        // 在进入最后的直航线前的转角前都有可能触发位移
        if(46 >= thechess.step) {
            if(thechess.color == MapData[Route[this.theround%4][thechess.step]].color) {
                if(18 == thechess.step) {
                    thechess.step += 12;
                }
                else {
                    thechess.step += 4;
                }
                jump1 = setTimeout(()=> {
                    thechess.x = MapData[Route[this.theround%4][thechess.step]].x;
                    thechess.y = MapData[Route[this.theround%4][thechess.step]].y;
                    thechess.angle = MapData[Route[this.theround%4][thechess.step]].angle;
                    // 第二次踩棋子
                    this.complex(thechess);
                    if(18 == thechess.step) {
                        jump2 = setTimeout(()=> {
                            thechess.step += 12;
                            thechess.x = MapData[Route[this.theround%4][thechess.step]].x;
                            thechess.y = MapData[Route[this.theround%4][thechess.step]].y;
                            thechess.angle = MapData[Route[this.theround%4][thechess.step]].angle;
                            // 第三次踩棋子
                            this.complex(thechess);
                        }, 500);
                    }
                }, 500);
            }
        }
    },

    // 重新开始游戏
    restart() {
        // 重置游戏其它变量
        clearInterval(onestep);
        temp = 0;
        forward = 0;
        backward = 0;
        clearTimeout(jump1);
        clearTimeout(jump2);
        clearTimeout(ruzhan);
        clearTimeout(changeturn);
        this.roundtitle = "";
        this.theround = 0;
        this.dice_pic = "dice";
        this.dice_num = 0;
        this.dice_dab = false;
        this.result = false;

        // 重置地图
        for(var i=0; i<MapData.length; i++) {
            MapData[i].chess = [];
        }

        // 重置飞行记录和排行榜
        for(var j=0; j<4; j++) {
            this.flylog[j].hit = 0;
            this.flylog[j].progress = 0;
        }
        this.allrank = [];

        // 重置棋子
        for(var k=0; k<4; k++) {
            this.RED[k].type = "wait";
            this.RED[k].chess_dab = true;
            this.RED[k].step = null;
            this.RED[k].x = MapData[this.RED[k].index].x;
            this.RED[k].y = MapData[this.RED[k].index].y;
            this.RED[k].angle = MapData[this.RED[k].index].angle;

            this.GREEN[k].type = "wait";
            this.GREEN[k].chess_dab = true;
            this.GREEN[k].step = null;
            this.GREEN[k].x = MapData[this.GREEN[k].index].x;
            this.GREEN[k].y = MapData[this.GREEN[k].index].y;
            this.GREEN[k].angle = MapData[this.GREEN[k].index].angle;

            this.YELLOW[k].type = "wait";
            this.YELLOW[k].chess_dab = true;
            this.YELLOW[k].step = null;
            this.YELLOW[k].x = MapData[this.YELLOW[k].index].x;
            this.YELLOW[k].y = MapData[this.YELLOW[k].index].y;
            this.YELLOW[k].angle = MapData[this.YELLOW[k].index].angle;

            this.BLUE[k].type = "wait";
            this.BLUE[k].chess_dab = true;
            this.BLUE[k].step = null;
            this.BLUE[k].x = MapData[this.BLUE[k].index].x;
            this.BLUE[k].y = MapData[this.BLUE[k].index].y;
            this.BLUE[k].angle = MapData[this.BLUE[k].index].angle;
        }

        // 棋子归位
        for(var l=0; l<4; l++) {
            MapData[77+l].chess.push(this.RED[l]);
            MapData[82+l].chess.push(this.GREEN[l]);
            MapData[87+l].chess.push(this.YELLOW[l]);
            MapData[92+l].chess.push(this.BLUE[l]);
        }

        // 默认红色先手
        thetype = this.RED;
        this.roundtitle = "红色方的回合";
    },
}